package implement

import (
	"context"
	"time"

	"gitlab.com/o-cloud/provider-mongodb-example/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// TODO : Load provider client and attach it to a global variable available through geter method
//        For exemple can be a mongodb client or ftp access.
func LoadProviderClients() {
	LoadMongoClient()
}

var mongoClient *mongo.Database

func LoadMongoClient() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(config.Config.Service.MongoConString))
	if err != nil {
		panic("Error init MongoDB client: " + err.Error())
	}

	mongoClient = client.Database(config.Config.Service.MongoDatabaseName)
}

func GetMongoClient() *mongo.Database {
	return mongoClient
}
