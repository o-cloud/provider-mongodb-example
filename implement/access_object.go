package implement

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"gitlab.com/o-cloud/provider-library/models/access_object"
	"gitlab.com/o-cloud/provider-library/models/permissions"
	"gitlab.com/o-cloud/provider-library/models/scopes"
	"gitlab.com/o-cloud/provider-library/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// DO NOT TOUCH
type accessObjectController struct {
}

// DO NOT TOUCH
func RegisterAccessObjectRepository() {
	accessObjectController := &accessObjectController{}
	access_object.SetAccessObjectController(accessObjectController)
}

// TODO : Create access object struct with all informations needed to connect on provider
type mongoDbAccess struct {
	ConnString string `json:"connString"`
	User       string `json:"user"`
	Role       string `json:"role"`
}

// TODO : Implement CreateAccess method that generate and return access object defined before
// You must not touch to method declaration
func (a *accessObjectController) GenerateAccess(scope *scopes.Scope, identities []string) access_object.ConnectionInformation {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	db := "providers"
	host := "mongo-mongodb:27017"
	roleName := identities[0] + "Role"
	userName := identities[0]
	pwd := utils.GenerateRandomStrongPassword()
	readOnlyAction := "find"

	privileges := []bson.M{}
	for _, resource := range scope.Resources {
		for _, permission := range scope.Permissions {
			switch permission {
			case permissions.AccessReadOnly:
				privilegeDoc := bson.M{
					"resource": bson.M{"db": db, "collection": resource.Name},
					"actions":  bson.A{readOnlyAction},
				}
				privileges = append(privileges, privilegeDoc)

			default:
				err := fmt.Errorf("permission %s not allowed", permission)
				panic(err)
			}
		}
	}

	roleResult := GetMongoClient().RunCommand(ctx, bson.D{
		{Key: "createRole", Value: roleName},
		{Key: "privileges", Value: privileges},
		{Key: "roles", Value: []bson.M{}},
	})
	err := roleResult.Err()
	switch t := err.(type) {
	case mongo.CommandError:
		if t.HasErrorCode(51002) {
			roleResult = GetMongoClient().RunCommand(ctx, bson.D{
				{Key: "updateRole", Value: roleName},
				{Key: "privileges", Value: privileges},
				{Key: "roles", Value: []bson.M{}},
			})
			err = roleResult.Err()
		}
	}
	if err != nil {
		panic(err)
	}

	userResult := GetMongoClient().RunCommand(ctx, bson.D{
		{Key: "createUser", Value: userName},
		{Key: "pwd", Value: pwd},
		{Key: "roles", Value: []bson.M{{"role": roleName, "db": db}}},
	})
	err = userResult.Err()
	switch t := err.(type) {
	case mongo.CommandError:
		if t.HasErrorCode(51003) {
			userResult := GetMongoClient().RunCommand(ctx, bson.D{
				{Key: "updateUser", Value: userName},
				{Key: "pwd", Value: pwd},
				{Key: "roles", Value: []bson.M{{"role": roleName, "db": db}}},
			})
			err = userResult.Err()
		}
	}
	if err != nil {
		panic(err)
	}

	mongoAccess := &mongoDbAccess{
		ConnString: fmt.Sprintf("mongodb://%s:%s@%s/%s?authSource=%s", userName, pwd, host, db, db),
		User:       userName,
		Role:       roleName,
	}

	return mongoAccess
}

// TODO : Implement PurgeAccess method that purge access that is represented by ConnectionInformation
// You must not touch to method declaration
func (a *accessObjectController) PurgeAccess(connInfo access_object.ConnectionInformation) {
	var mongoAccess mongoDbAccess
	if err := json.Unmarshal(connInfo.([]byte), &mongoAccess); err != nil {
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	userResult := GetMongoClient().RunCommand(ctx, bson.D{{Key: "dropUser", Value: mongoAccess.User}})
	err := userResult.Err()
	switch t := err.(type) {
	case mongo.CommandError:
		if t.HasErrorCode(11) {
			return
		}
	}

	if err != nil {
		panic(userResult.Err())
	}

	roleResult := GetMongoClient().RunCommand(ctx, bson.D{{Key: "dropRole", Value: mongoAccess.Role}})
	err = roleResult.Err()
	switch t := err.(type) {
	case mongo.CommandError:
		log.Println(t.Code)
		if t.HasErrorCode(11) {
			return
		}
	}

	if err != nil {
		panic(roleResult.Err())
	}
}
