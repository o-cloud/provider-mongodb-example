package main

import (
	"gitlab.com/o-cloud/provider-library/provider"
	"gitlab.com/o-cloud/provider-mongodb-example/config"
	"gitlab.com/o-cloud/provider-mongodb-example/implement"
)

func main() {
	config.Load()
	provider.InitProvider(config.Config.Provider)

	implement.LoadProviderClients()
	implement.RegisterAccessPermission()
	implement.RegisterResourcesRepository()
	implement.RegisterAccessObjectRepository()

	provider.RunProvider()
}
