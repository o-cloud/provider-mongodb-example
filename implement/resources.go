package implement

import (
	"context"

	"gitlab.com/o-cloud/provider-library/models/resources"
	"go.mongodb.org/mongo-driver/bson"
)

// DO NOT TOUCH
type resourcesRepository struct {
	resources.DefaultResourcesRepository
}

// DO NOT TOUCH
func RegisterResourcesRepository() {
	resourcesRepository := &resourcesRepository{}
	resourcesRepository.ResourcesRepository = resourcesRepository
	resources.CreateResourcesRepository(resourcesRepository)
}

// TODO : Implement ListResources method that return list of available resources
//        in provider configuration of scopes
// You must not touch to method declaration
func (r *resourcesRepository) ListResources() *[]resources.Resource {
	avResources := []resources.Resource{}

	// return resources list (get all the mongo db collection for example)
	result, err := GetMongoClient().ListCollectionNames(context.TODO(), bson.D{})

	if err != nil {
		panic(err)
	}

	for _, coll := range result {
		resource := resources.Resource{Name: coll}
		avResources = append(avResources, resource)
	}

	return &avResources
}
